package ir.bund.project1.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ir.bund.project1.R;
import ir.bund.project1.app.EndPoints;
import ir.bund.project1.app.MyApplication;
import ir.bund.project1.app.PrefManager;
import ir.bund.project1.web_service.OkHttpBuilder;
import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by AmirReza Erfanin on 23/08/2016.
 */


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.currentActivity = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final JSONArray jsonArray = MyApplication.prefManager.getData();
        int j = jsonArray.length();
        for (int i = j; i>0; i--) {
            jsonArray.remove(i);
            final int finalI = i;
            MyApplication.handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        setData(jsonArray.getJSONObject(finalI));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, 2000);
        }
        MyApplication.prefManager.setDatas(jsonArray.toString());

        Log.i("LOG", "onCreate: " + MyApplication.prefManager.getData().toString());
        final RadioButton male = (RadioButton) findViewById(R.id.rbtnMale);
        final RadioGroup rGroup = (RadioGroup) findViewById(R.id.rgroSex);
        final EditText edtTall = (EditText) findViewById(R.id.edtTall);
        final EditText edtWeight = (EditText) findViewById(R.id.edtWeight);
        final EditText edtAge = (EditText) findViewById(R.id.edtAge);
        Button btnAccept = (Button) findViewById(R.id.btnAccept);
        final TextInputLayout tilTall = (TextInputLayout) findViewById(R.id.tilTall);


        btnAccept.setOnClickListener(new View.OnClickListener() {

                                         @Override
                                         public void onClick(View v) {

                                             // get selected radio button from radioGroup

                                             String tall = edtTall.getText().toString();
                                             String weight = edtWeight.getText().toString();
                                             String age = edtAge.getText().toString();

                                             if (tall.trim().isEmpty()) {
                                                 tilTall.setError("قد را وارد نمایید");
                                                 edtTall.requestFocus();
                                                 return;
                                             }
                                             JSONObject object = new JSONObject();

                                             int selectedId = rGroup.getCheckedRadioButtonId();
                                             try {
                                                 if (selectedId == male.getId()) {
                                                     object.put("Gender", "male");
                                                 } else {
                                                     object.put("Gender", "female");
                                                 }
                                                 object.put("length", tall);
                                                 object.put("weight", weight);
                                                 object.put("age", age);

                                             } catch (JSONException e) {
                                                 e.printStackTrace();
                                             }


                                             setData(object);
                                         }
                                     }
        );
    }

    private static void setData(final JSONObject object) {
        Log.i("LOG", "setData: "+object);
        HttpUrl.Builder builder = HttpUrl.parse(EndPoints.SEND_DATA).newBuilder();

        try {
            builder.addQueryParameter("Gender", object.getString("Gender"));
            builder.addQueryParameter("length", object.getString("length"));
            builder.addQueryParameter("age", object.getString("age"));
            builder.addQueryParameter("weight", object.getString("weight"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        OkHttpBuilder okHttpBuilder = new OkHttpBuilder();
        okHttpBuilder.callback(new OkHttpBuilder.Callback() {
            @Override
            public void onResponse(Call call, String data) {
                Log.i("LOG", "onResponse: " + data);
                MyApplication.handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MyApplication.context, "اطلاعات با موفقیت ثبت گردید", Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void onFailure(final Call call, int errCode) {
                Log.i("LOG", "onResponse errorCode : " + errCode);
                MyApplication.handler.post(new Runnable() {
                    @Override
                    public void run() {
                        MyApplication.prefManager.setData(object);
                        Log.i("LOG", "run: "+ MyApplication.prefManager.getData());
                        Toast.makeText(MyApplication.context, "اطلاعات با موفقیت در حافظه ثبت گردید", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        MediaType type = MediaType.parse("application/json; charset=utf-8");

        okHttpBuilder.formBody(RequestBody.create(type,object.toString()),EndPoints.SEND_DATA);
//        okHttpBuilder.urlBuilder(builder);
        okHttpBuilder.call();
    }

}
