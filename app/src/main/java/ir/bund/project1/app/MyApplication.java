package ir.bund.project1.app;


import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;
import java.util.Locale;
/**
 * Created by AmirReza Erfanin on 23/08/2016.
 */

public class MyApplication extends Application {

    public static Context context;
    public static Activity currentActivity;
    public static Handler handler;
    public static PrefManager prefManager;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        context = getApplicationContext();
        prefManager = new PrefManager(context);

    }


}
