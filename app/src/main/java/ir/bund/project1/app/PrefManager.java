package ir.bund.project1.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by AmirReza Erfanin on 23/08/2016.
 */
public class PrefManager {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "AE_P_FOR_MK";

    // All Shared Preferences Keys
    private static final String KEY_DATA = "data";




    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setData(JSONObject object) {
        editor.putString(KEY_DATA, getData().put(object).toString());
        editor.commit();
    }
    public void setDatas(String value) {
        editor.putString(KEY_DATA, value);
        editor.commit();
    }


    public JSONArray getData() {
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(pref.getString(KEY_DATA, "[]"));
        } catch (JSONException e) {
            e.printStackTrace();
            jsonArray = new JSONArray();
        }
        return jsonArray;
    }

    public void deletePref()
    {
        Editor editor = pref.edit();
        editor.clear();
        editor.apply();
    }

}
