package ir.bund.project1.web_service;

import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/***
 * Created by AmirReza Erfanian on 1/6/17.
 * <p>
 * <p>
 * Add these line to build.gradle :
 * <p>
 * compile 'com.squareup.okhttp3:okhttp:+'
 * compile 'com.squareup.okio:okio:1.11.0'
 * <p>
 * <p>
 */

public class OkHttpBuilder implements Callback {


  private final String TAG = OkHttpBuilder.class.getSimpleName();

  public static final int INTERNET_CONNECTION_EXCEPTION = -1;
  public static final int SERVER_PROBLEM_EXCEPTION = -2;

  private Callback callback;
  private int connectionTimeout = 10;
  private int writeTimeout = 10;
  private int readTimeout = 20;
  private HttpUrl.Builder urlBuilder = null;
  /**
   * RequestBody formBody = new FormBody.Builder()
   * .add("message", "Your message")
   * .build();
   */
  private RequestBody formBody = null;
  private String url;
  private boolean deleteNameSpace = true;


  public interface Callback {
    void onResponse(Call call, String data);

    void onFailure(Call call, int errCode);
  }

  public OkHttpBuilder callback(Callback callback) {
    this.callback = callback;
    return this;
  }

  public OkHttpBuilder connectionTimeout(int connectionTimeout) {
    this.connectionTimeout = connectionTimeout;
    return this;
  }

  public OkHttpBuilder writeTimeout(int writeTimeout) {
    this.writeTimeout = writeTimeout;
    return this;
  }

  public OkHttpBuilder formBody(RequestBody formBody,String url) {
    this.formBody = formBody;
    this.url = url;
    return this;
  }


  public OkHttpBuilder readTimeout(int readTimeout) {
    this.readTimeout = readTimeout;
    return this;
  }

  /**
   * @param urlBuilder is a HttpUrl.Builder Example :
   *                   HttpUrl.Builder urlBuilder = HttpUrl
   *                   .parse("http://94.183.246.21:8008/HyundaiP/HyundaiP.asmx/CustomerGetProfile")
   *                   .newBuilder();
   *                   urlBuilder.addQueryParameter("phoneNumber", "0");
   * @return OkHttpBuilder instance
   */
  public OkHttpBuilder urlBuilder(HttpUrl.Builder urlBuilder) {
    this.urlBuilder = urlBuilder;
    return this;
  }

  public OkHttpBuilder deleteNameSpace(boolean deleteNameSpace) {
    this.deleteNameSpace = deleteNameSpace;
    return this;
  }


  public void call() {

    OkHttpClient client = new OkHttpClient.Builder()
      .connectTimeout(connectionTimeout, TimeUnit.SECONDS)
      .writeTimeout(writeTimeout, TimeUnit.SECONDS)
      .readTimeout(readTimeout, TimeUnit.SECONDS)
      .build();

    Request request;

    if (urlBuilder == null) {
      request = new Request.Builder()
              .url(url)
              .post(formBody)
              .build();
    } else {
      String url = urlBuilder.build().toString();
      request = new Request.Builder()
        .url(url)
        .build();
    }

    client.newCall(request)
      .enqueue(this);

  }

  private Handler handler = new Handler();

  @Override
  public void onFailure(final Call call, IOException e) {
    if (callback != null)
      handler.post(new Runnable() {
        @Override
        public void run() {
          callback.onFailure(call, INTERNET_CONNECTION_EXCEPTION);
        }
      });
  }

  @Override
  public void onResponse(final Call call, Response response) throws IOException {
    String bodyStr = response.body().string();
    Log.i(TAG, "onResponse: "+response);
    if (!response.isSuccessful()) {
      if (callback != null){
        handler.post(new Runnable() {
          @Override
          public void run() {
            callback.onFailure(call, SERVER_PROBLEM_EXCEPTION);
          }
        });
      }
      Log.e(TAG, "onResponse:error " + bodyStr);
      throw new IOException("Unexpected code " + response);
    }


    if (deleteNameSpace) {
      bodyStr = parseHelper(bodyStr);
    }

    if (callback != null) {
      final String finalBodyStr = bodyStr;
      handler.post(new Runnable() {
        @Override
        public void run() {
          callback.onResponse(call, finalBodyStr);
        }
      });
    }
  }


  private String parseHelper(String str) {
    str = str.intern().trim();
    Pattern pattern = Pattern.compile("\\<.*?\\>");
    Matcher matcher = pattern.matcher(str);

    return matcher.replaceAll("").trim();
  }

}
